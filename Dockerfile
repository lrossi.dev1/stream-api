FROM openjdk:17-oracle

LABEL "maintainer"="Leonardo Rossi"

COPY ./build/libs/stream-api*SNAPSHOT.jar /app/stream-api.jar

ENTRYPOINT ["java", "-jar", "/app/stream-api.jar"]