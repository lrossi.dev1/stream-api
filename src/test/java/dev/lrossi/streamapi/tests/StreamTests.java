package dev.lrossi.streamapi.tests;

import dev.lrossi.streamapi.BaseTest;
import dev.lrossi.streamapi.dto.request.VideoRequest;
import dev.lrossi.streamapi.model.Video;
import dev.lrossi.streamapi.model.VideoFile;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@RunWith(SpringRunner.class)
public class StreamTests extends BaseTest {

    @Autowired
    private ReactiveMongoTemplate reactiveMongoTemplate;

    @AfterEach
    public void cleanUpEach() {
        reactiveMongoTemplate.dropCollection(Video.class).block();
        reactiveMongoTemplate.dropCollection(VideoFile.class).block();
    }

    @Test
    public void uploadAValidVideoShouldReturnCreated() {
        File validFile = new File(getClass().getClassLoader().getResource("files/test_video.mp4").getFile());
        RestAssured.given()
                .multiPart("file", validFile)
                .basePath("/videos/file")
            .when()
                .post()
            .then()
                .statusCode(201)
                .header("Location", Matchers.containsString(RestAssured.basePath));
    }

    @Test
    public void addValidMetadataToExistingFileShouldReturnOk() {
        final VideoRequest validVideoRequest = stubForValidVideoMetadata();
        final VideoFile videoFile = stubForValidVideoFile();
        reactiveMongoTemplate.save(videoFile).block();

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(validVideoRequest)
                .basePath("/videos")
            .when()
                .post()
            .then()
                .statusCode(201)
                .header("Location", Matchers.containsString("/videos"));
    }

    @Test
    public void addValidMetadataToNonExistingFileShouldReturnBadRequest() {
        final VideoRequest validVideoRequest = stubForValidVideoMetadata();
        // Here we didn't save the file, so it doesn't exist

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(validVideoRequest)
                .basePath("/videos")
            .when()
                .post()
            .then()
                .statusCode(400)
                .body("message",
                        Matchers.equalTo(String.format("File with id %s not found", validVideoRequest.getVideoFileId()))
                );
    }

    @Test
    public void addInvalidMetadataToExistingFileShouldReturnBadRequest() {
        final VideoRequest invalidVideoRequest = stubForInvalidVideoRequest();
        final VideoFile validVideoFile = stubForValidVideoFile();
        reactiveMongoTemplate.save(validVideoFile).block();

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(invalidVideoRequest)
                .basePath("/videos")
            .when()
                .post()
            .then()
                .statusCode(400)
                .body("message", Matchers.equalTo("Video title can't be empty"));
    }

    @Test
    public void getAllVideosShouldReturnOk() {
        final VideoFile validVideoFile = stubForValidVideoFile();
        final Video validVideo = stubForValidVideo();
        final Video secondVideo = stubForValidVideo();
        secondVideo.setId("xyz999");

        reactiveMongoTemplate.save(validVideoFile)
                .then(reactiveMongoTemplate.save(validVideo))
                .then(reactiveMongoTemplate.save(secondVideo))
                .block();

        RestAssured.given()
                .basePath("/videos")
            .when()
                .get()
            .then()
                .statusCode(200)
                .body("$", Matchers.hasSize(2));
    }

    @Test
    public void searchForVideoWithValidCriteriaShouldReturnOk() {
        final VideoFile validVideoFile = stubForValidVideoFile();
        final Video validVideo = stubForValidVideo();
        final Video secondVideo = stubForValidVideo();
        secondVideo.setId("xyz999");
        secondVideo.setDirector("Martin Testese");

        reactiveMongoTemplate.save(validVideoFile)
                .then(reactiveMongoTemplate.save(validVideo))
                .then(reactiveMongoTemplate.save(secondVideo))
                .block();

        RestAssured.given()
                .basePath("/videos")
                .queryParam("director", "Myself")
            .when()
                .get()
            .then()
                .statusCode(200)
                .body("$", Matchers.hasSize(1))
                .body("[0].id", Matchers.equalTo(validVideo.getId()));
    }

    @Test
    public void searchForVideoWithInvalidCriteriaShouldReturnOkWithAnEmptyList() {
        final VideoFile validVideoFile = stubForValidVideoFile();
        final Video validVideo = stubForValidVideo();
        final Video secondVideo = stubForValidVideo();
        secondVideo.setId("xyz999");
        secondVideo.setDirector("Martin Testese");

        reactiveMongoTemplate.save(validVideoFile)
                .then(reactiveMongoTemplate.save(validVideo))
                .then(reactiveMongoTemplate.save(secondVideo))
                .block();

        RestAssured.given()
                .basePath("/videos")
                .queryParam("director", "Unknown")
            .when()
                .get()
            .then()
                .statusCode(200)
                .body("$", Matchers.hasSize(0));
    }

    @Test
    public void getVideoDetailsByValidIdShouldReturnOk() {
        final VideoFile validVideoFile = stubForValidVideoFile();
        final Video validVideo = stubForValidVideo();
        final Video secondVideo = stubForValidVideo();
        secondVideo.setId("xyz999");
        secondVideo.setDirector("Martin Testese");

        reactiveMongoTemplate.save(validVideoFile)
                .then(reactiveMongoTemplate.save(validVideo))
                .then(reactiveMongoTemplate.save(secondVideo))
                .block();

        RestAssured.given()
                .basePath("/videos/" + validVideo.getId())
            .when()
                .get()
            .then()
                .statusCode(200)
                .body("id", Matchers.equalTo(validVideo.getId()));
    }

    @Test
    public void getVideoDetailsByInvalidIdShouldReturnNotFound() {
        final VideoFile validVideoFile = stubForValidVideoFile();
        final Video validVideo = stubForValidVideo();
        final Video secondVideo = stubForValidVideo();
        secondVideo.setId("xyz999");
        secondVideo.setDirector("Martin Testese");

        reactiveMongoTemplate.save(validVideoFile)
                .then(reactiveMongoTemplate.save(validVideo))
                .then(reactiveMongoTemplate.save(secondVideo))
                .block();

        RestAssured.given()
                .basePath("/videos/invalid999")
            .when()
                .get()
            .then()
                .statusCode(404);
    }


    @Test
    public void loadAValidVideoShouldReturnOk() throws IOException {
        final VideoFile validVideoFile = stubForValidVideoFile();
        reactiveMongoTemplate.save(validVideoFile).block();

        InputStream inputStream = RestAssured.given()
                .basePath("/videos/file/" + validVideoFile.getId())
            .when()
                .get()
            .then()
                .statusCode(200)
                .extract().response().asInputStream();

        File validFile = new File(getClass().getClassLoader().getResource("files/test_video.mp4").getFile());
        Assert.assertEquals(validFile.length(), inputStream.available());
    }

    @Test
    public void loadAnInvalidVideoShouldReturnNotFound() {
        RestAssured.given()
                .basePath("/videos/file/invalidid999")
            .when()
                .get()
            .then()
                .statusCode(404);
    }

    @Test
    public void deleteVideoShouldReturnNoContent() {
        final VideoFile validVideoFile = stubForValidVideoFile();
        final Video validVideo = stubForValidVideo();
        reactiveMongoTemplate.save(validVideoFile)
                .then(reactiveMongoTemplate.save(validVideo))
                .block();

        RestAssured.given()
                .basePath("/videos/" + validVideo.getId())
            .when()
                .delete()
            .then()
                .statusCode(204);

        reactiveMongoTemplate.findById(validVideo.getId(), Video.class)
                .doOnNext(video -> Assert.assertEquals(video.getActive(), false))
                .block();
    }

    private static VideoRequest stubForValidVideoMetadata() {
        return VideoRequest.builder()
                .title("Some Video")
                .director("Mr. Myself")
                .genre("Drama")
                .synopsis("Some dramatic video to get streamed")
                .cast("Mr. Myself, Mr. No One Else")
                .runningTime(33)
                .yearOfRelease(2023)
                .videoFileId("abc123")
                .build();
    }

    private static VideoRequest stubForInvalidVideoRequest() {
        return VideoRequest.builder().videoFileId("abc123").build();
    }

    private static VideoFile stubForValidVideoFile() {
        return VideoFile.builder()
                .id("abc123")
                .fileURI("test_video.mp4").build();
    }

    private static VideoFile stubForInvalidVideoFile() {
        return VideoFile.builder()
                .id("invalidId")
                .fileURI("https://some.remote.file.com")
                .build();
    }


    private static Video stubForValidVideo() {
        return Video.builder()
                .id("abc123")
                .title("Some Video")
                .director("Mr. Myself")
                .genre("Drama")
                .synopsis("Some dramatic video to get streamed")
                .cast("Mr. Myself, Mr. No One Else")
                .runningTime(33)
                .yearOfRelease(2023)
                .active(true)
                .videoFile(stubForValidVideoFile())
                .build();
    }
}
