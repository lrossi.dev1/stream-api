package dev.lrossi.streamapi.handler;

import dev.lrossi.streamapi.exception.InvalidParameterException;
import dev.lrossi.streamapi.exception.NotFoundException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.support.WebExchangeBindException;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(InvalidParameterException.class)
    public Mono<ResponseEntity<?>> handleInvalidParameterException(final InvalidParameterException ex) {
        return Mono.just(ResponseEntity
                .badRequest()
                .body(Map.of("message", ex.getMessage())));
    }

    @ExceptionHandler(WebExchangeBindException.class)
    public Mono<ResponseEntity<?>> handleValidationException(WebExchangeBindException e) {
        return Mono.just(ResponseEntity
                .badRequest()
                .body(Map.of("message", e.getBindingResult().getFieldError().getDefaultMessage())));
    }

    @ExceptionHandler(NotFoundException.class)
    public Mono<ResponseEntity<?>> handleNotFoundException(NotFoundException e) {
        return Mono.just(ResponseEntity
                .notFound()
                .build());
    }
}
