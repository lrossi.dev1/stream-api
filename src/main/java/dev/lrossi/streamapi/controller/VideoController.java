package dev.lrossi.streamapi.controller;

import dev.lrossi.streamapi.dto.request.VideoRequest;
import dev.lrossi.streamapi.dto.response.VideoResponse;
import dev.lrossi.streamapi.dto.response.VideoSearchResultResponse;
import dev.lrossi.streamapi.service.VideoService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.List;

@RestController
public class VideoController {
    private static final String BASE_PATH = "/videos";

    private VideoService videoService;

    @Autowired
    public VideoController(final VideoService videoService) {
        this.videoService = videoService;
    }

    @PostMapping(value = BASE_PATH, consumes = "application/json", produces = "application/json")
    public Mono<ResponseEntity<Void>> create(@RequestBody @Valid final VideoRequest videoRequest) {
        return videoService.create(videoRequest)
                .map(id -> ResponseEntity.created(URI.create(BASE_PATH + "/" + id)).build());
    }

    @GetMapping(value = BASE_PATH, produces = "application/json")
    public Mono<ResponseEntity<List<VideoSearchResultResponse>>> findVideoByCriteria(@RequestParam(value = "title", required = false) final String title,
                                                                                     @RequestParam(value = "director", required = false) final String director,
                                                                                     @RequestParam(value = "synopsis", required = false) final String synopsis) {
        return videoService.getAllVideosBySpec(title, director, synopsis)
                .map(ResponseEntity::ok);

    }

    @GetMapping(value = BASE_PATH + "/{id}", produces = "application/json")
    public Mono<ResponseEntity<VideoResponse>> findByid(@PathVariable final String id) {
        return videoService.findById(id)
                .map(ResponseEntity::ok);
    }

    @PatchMapping(value = BASE_PATH, consumes = "application/json", produces = "application/json")
    public Mono<ResponseEntity<Void>> update(@RequestBody @Valid final VideoRequest videoRequest) {
        return videoService.update(videoRequest)
                .map(videoId -> ResponseEntity.noContent().build());
    }

    @DeleteMapping(BASE_PATH + "/{id}")
    public Mono<ResponseEntity<Void>> delete(@PathVariable final String id) {
        return videoService.delete(id)
                .map(videoId -> ResponseEntity.noContent().build());
    }
}
