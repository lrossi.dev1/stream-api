package dev.lrossi.streamapi.controller;

import dev.lrossi.streamapi.service.VideoFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.net.URI;

@RestController
public class VideoFileController {

    private static final String BASE_PATH = "/videos/file";

    private final Logger logger = LoggerFactory.getLogger(VideoFileController.class);

    private VideoFileService videoFileService;

    @Autowired
    public VideoFileController(final VideoFileService videoFileService) {
        this.videoFileService = videoFileService;
    }

    @PostMapping(value = BASE_PATH, produces = "application/json", consumes = "multipart/form-data")
    public Mono<ResponseEntity<Void>> uploadFile(@RequestPart final Mono<FilePart> file) {
        return videoFileService.create(file)
                .map(fileId -> ResponseEntity.created(URI.create(String.format("%s/%s", BASE_PATH, fileId))).build());
    }

    @GetMapping(value = BASE_PATH + "/{id}", produces = "video/mp4")
    public Mono<Resource> getFileById(@PathVariable final String id) {
        return videoFileService.getFile(id);
    }
}
