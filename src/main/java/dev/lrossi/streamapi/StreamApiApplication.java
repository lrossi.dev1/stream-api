package dev.lrossi.streamapi;

import dev.lrossi.streamapi.repository.FileStorage;
import jakarta.annotation.Resource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamApiApplication {

	@Resource
	private FileStorage fileStorage;

	public static void main(String[] args) {
		SpringApplication.run(StreamApiApplication.class, args);
	}

}
