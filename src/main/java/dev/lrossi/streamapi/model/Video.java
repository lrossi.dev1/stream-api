package dev.lrossi.streamapi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document
@AllArgsConstructor
@NoArgsConstructor
public class Video {

    @Id
    private String id;

    private String title;

    private String synopsis;

    private String director;

    private String cast;

    private Integer yearOfRelease;

    private String genre;

    private Integer runningTime;

    private VideoFile videoFile;

    private Boolean active;

}
