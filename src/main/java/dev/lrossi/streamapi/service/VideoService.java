package dev.lrossi.streamapi.service;

import dev.lrossi.streamapi.dto.request.VideoRequest;
import dev.lrossi.streamapi.dto.response.VideoResponse;
import dev.lrossi.streamapi.dto.response.VideoSearchResultResponse;
import dev.lrossi.streamapi.exception.InvalidParameterException;
import dev.lrossi.streamapi.exception.NotFoundException;
import dev.lrossi.streamapi.model.Video;
import dev.lrossi.streamapi.model.VideoFile;
import dev.lrossi.streamapi.repository.VideoFileRepository;
import dev.lrossi.streamapi.repository.VideoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Objects;

@Service
public class VideoService {

    private final Logger logger = LoggerFactory.getLogger(VideoService.class);

    private VideoRepository videoRepository;

    private VideoFileRepository videoFileRepository;

    @Autowired
    public VideoService(final VideoRepository videoRepository, final VideoFileRepository videoFileRepository) {
        this.videoRepository = videoRepository;
        this.videoFileRepository = videoFileRepository;
    }

    public Mono<String> create(final VideoRequest videoRequest) {
        return videoFileRepository.findById(videoRequest.getVideoFileId())
                .switchIfEmpty(Mono.error(new InvalidParameterException("File with id " + videoRequest.getVideoFileId() + " not found")))
                .zipWith(Mono.just(videoRequestToVideo(videoRequest)))
                .map(tuple -> {
                    final VideoFile videoFile = tuple.getT1();
                    final Video video = tuple.getT2();
                    video.setVideoFile(videoFile);
                    video.setActive(true);
                    return video;
                })
                .flatMap(videoRepository::save)
                .map(Video::getId)
                .doOnSuccess(id -> logger.info("Metadata for video {} saved", id))
                .doOnError(err -> logger.error("Error while saving video metadata.", err));
    }

    public Mono<List<VideoSearchResultResponse>> getAllVideosBySpec(final String title,
                                                                    final String director,
                                                                    final String synopsis) {
        Flux<Video> queryResults = videoRepository.findByActiveTrue();

        if (Objects.nonNull(title) || Objects.nonNull(director) || Objects.nonNull(synopsis)) {
            final ExampleMatcher exampleMatcher = ExampleMatcher.matching()
                    .withIgnoreCase()
                    .withMatcher("title", ExampleMatcher.GenericPropertyMatchers.contains())
                    .withMatcher("director", ExampleMatcher.GenericPropertyMatchers.contains())
                    .withMatcher("synopsis", ExampleMatcher.GenericPropertyMatchers.contains());

            final Video example = Video.builder()
                    .title(title)
                    .director(director)
                    .synopsis(synopsis)
                    .active(true).build();

            queryResults = Mono.just(exampleMatcher)
                    .zipWith(Mono.just(example))
                    .flux()
                    .flatMap(tuple -> videoRepository.findAll(Example.of(example, exampleMatcher)));
        }

        return queryResults
                .map(video -> {
                    VideoSearchResultResponse videoResponse = new VideoSearchResultResponse();
                    BeanUtils.copyProperties(video, videoResponse);
                    return videoResponse;
                }).collectList()
                .doOnSuccess(videos -> logger.info("Videos successfully retrieved. query[title={},director={},synopsis={}]", title, director, synopsis))
                .doOnError(err -> logger.error("Error while retrieving all videos. query[title={},director={},synopsis={}]", title, director, synopsis, err));
    }

    public Mono<VideoResponse> findById(final String id) {
        return videoRepository.findByIdAndActiveTrue(id)
                .switchIfEmpty(Mono.error(new NotFoundException("Video not found")))
                .map(video -> {
                    VideoResponse videoResponse = new VideoResponse();
                    BeanUtils.copyProperties(video, videoResponse);
                    videoResponse.setVideoFile(video.getVideoFile().getId());
                    return videoResponse;
                })
                .doOnSuccess(videoResponse -> logger.info("Video {} successfully retrieved", videoResponse.getId()))
                .doOnError(err -> logger.error("Error while retrieving video {}", id, err));
    }

    public Mono<String> update(final VideoRequest videoRequest) {
        return Mono.just(videoRequest)
                .map(this::videoRequestToVideo)
                .flatMap(videoRepository::save)
                .map(Video::getId)
                .doOnSuccess(videoId -> logger.info("Video {} updated successfully", videoId))
                .doOnError(err -> logger.error("Error while updating video {}.", videoRequest.getId(), err));
    }

    public Mono<String> delete(final String id) {
        return videoRepository.findById(id)
                .switchIfEmpty(Mono.error(new NotFoundException("Video not found")))
                .map(video -> {
                    video.setActive(false);
                    return video;
                })
                .flatMap(videoRepository::save)
                .map(Video::getId)
                .doOnSuccess(video -> logger.info("Video {} succesfully deleted", id))
                .doOnError(err -> logger.error("Error while deleting video {}.", id));
    }


    private Video videoRequestToVideo(final VideoRequest videoRequest) {
        final Video video = new Video();
        BeanUtils.copyProperties(videoRequest, video);
        return video;
    }
}
