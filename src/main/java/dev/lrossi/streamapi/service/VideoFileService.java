package dev.lrossi.streamapi.service;

import dev.lrossi.streamapi.exception.NotFoundException;
import dev.lrossi.streamapi.model.VideoFile;
import dev.lrossi.streamapi.repository.FileStorage;
import dev.lrossi.streamapi.repository.VideoFileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class VideoFileService {

    private final Logger logger = LoggerFactory.getLogger(VideoFileService.class);

    private FileStorage fileStorage;

    private VideoFileRepository videoFileRepository;

    @Autowired
    public VideoFileService(final FileStorage fileStorage,
                            final VideoFileRepository videoFileRepository) {
        this.fileStorage = fileStorage;
        this.videoFileRepository = videoFileRepository;
    }

    public Mono<String> create(final Mono<FilePart> filePart) {
        return fileStorage.save(filePart)
                .flatMap(fileURI -> {
                    final VideoFile videoFile = VideoFile.builder().fileURI(fileURI).build();
                    return videoFileRepository.save(videoFile);
                })
                .map(VideoFile::getId)
                .doOnSuccess(videoFile -> logger.info("File {} successfully saved", videoFile))
                .doOnError(err -> logger.error("Error while saving file", err));
    }

    public Mono<Resource> getFile(final String id) {
        return videoFileRepository.findById(id)
                .switchIfEmpty(Mono.error(new NotFoundException("File not found")))
                .flatMap(videoFile -> fileStorage.load(videoFile.getFileURI()))
                .doOnSuccess(resource -> logger.info("Successfully streaming video {}", id))
                .doOnError(err -> logger.error("Error while streaming video {}", id, err));
    }
}
