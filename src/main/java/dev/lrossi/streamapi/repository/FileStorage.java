package dev.lrossi.streamapi.repository;

import org.springframework.core.io.Resource;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface FileStorage {

    Mono<String> save(Mono<FilePart> filePart);

    Mono<Resource> load(String filename);

}
