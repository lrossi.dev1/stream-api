package dev.lrossi.streamapi.repository;

import dev.lrossi.streamapi.model.Video;
import org.springframework.data.domain.Example;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.repository.query.ReactiveQueryByExampleExecutor;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface VideoRepository extends ReactiveMongoRepository<Video, String>, ReactiveQueryByExampleExecutor<Video> {

    Flux<Video> findByActiveTrue();

    Flux<Video> findByActiveTrue(Example<Video> example);

    Mono<Video> findByIdAndActiveTrue(String id);

}
