package dev.lrossi.streamapi.repository;

import dev.lrossi.streamapi.model.VideoFile;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VideoFileRepository extends ReactiveMongoRepository<VideoFile, String> {
}
