package dev.lrossi.streamapi.repository.impl;

import dev.lrossi.streamapi.repository.FileStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.nio.file.Path;

/**
 * Here we will store the files locally, but we could use something like a S3 Bucket as well
 */
@Component
public class LocalFileStorage implements FileStorage {

    private final Logger logger = LoggerFactory.getLogger(LocalFileStorage.class);

    @Value("${stream-api.root-dir}")
    private String destDir;

    @Value("${stream-api.source-dir}")
    private String sourceDir;

    private FileSystemResourceLoader resourceLoader;

    @Autowired
    public LocalFileStorage(final FileSystemResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @Override
    public Mono<String> save(final Mono<FilePart> filePart) {
        return filePart.flatMap(fp -> {
            String fileName = fp.filename();
            return fp.transferTo(Path.of(destDir).resolve(fileName))
                    .then(Mono.just(fileName));
        });
    }

    @Override
    public Mono<Resource> load(final String filePath) {
        return Mono.fromSupplier(() -> resourceLoader.getResource(sourceDir + filePath));
    }
}
