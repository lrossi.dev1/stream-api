package dev.lrossi.streamapi.dto.response;

import dev.lrossi.streamapi.model.VideoFile;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VideoResponse {

    private String id;

    private String title;

    private String synopsis;

    private String director;

    private String cast;

    private Integer yearOfRelease;

    private String genre;

    private Integer runningTime;

    private String videoFile;

}
