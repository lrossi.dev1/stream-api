package dev.lrossi.streamapi.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VideoSearchResultResponse {

    private String id;

    private String title;

    private String director;

    private String genre;

    private String synopsis;

    private Integer runningTime;

}
