package dev.lrossi.streamapi.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VideoRequest {

    private String id;

    @NotBlank(message = "Video title can't be empty")
    private String title;

    private String synopsis;

    private String director;

    private String cast;

    private Integer yearOfRelease;

    private String genre;

    private Integer runningTime;

    private String videoFileId;

}
