# stream-api

An API for playing around with video streaming a little bit.

## How do do I run i
You will need:
- JDK 17+;
- Either Docker or MongoDB installed;

You can either `./gradlew bootRun` in case you already have a running instance of MongoDB locally, or you can `./gradlew build && docker compose up` to start everything containerized.
## How do I use it?

After the app is up and running, just refer to http://localhost:8082/swagger-ui.html and check it out. There is an example video at `src/test/resources/files` if you don't have any video file lying around.

## How does it work?

Currently all of the videos are being stored locally, but the `FileStorage` implementation could be switched to a better alternative (such as an AWS S3 bucket client) with not a lot of effort.

